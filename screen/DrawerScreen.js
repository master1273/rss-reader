import React, {Component} from 'react';
import {ScrollView, Text, View, StyleSheet} from 'react-native';
import {NavigationActions, DrawerActions} from 'react-navigation';
import PropTypes from 'prop-types';

class DrawerScreen extends Component {
    navigateToScreen = (route) => () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
    };

    render () {
        return (
            <View>
                <ScrollView>
                    <View>
                        <View style={styles.menuItem}>
                            <Text onPress={this.navigateToScreen('home')}>
                                Home
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

DrawerScreen.propTypes = {
    navigation: PropTypes.object
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    heading: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    menuItem:{
        padding: 20,
        fontSize: 20,
        borderWidth: 0.5,
        borderColor: '#d6d7da'
    }
});

export default DrawerScreen;