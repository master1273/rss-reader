import React from 'react';
import { View, AsyncStorage, ScrollView, Text, Alert, RefreshControl, StyleSheet } from 'react-native';
import _ from 'underscore';
import RssList from "../component/RssList";

class HomeScreen extends React.Component {

    static navigationOptions = {
        title: 'Home'
    };

    constructor() {
        super();

        this.state = {
            rssUrl: '',
            rssList: []
        };

        this.getRssList();

        this.getRssList = this.getRssList.bind(this);
        this.handleDeleteRss = this.handleDeleteRss.bind(this);
    }

    getRssList() {
        AsyncStorage.getItem('rssList')
            .then((response) => {
                const rssList = JSON.parse(response) || [];
                this.setState({ rssList });
            });
    }

    handleDeleteRss(rssUrl) {
        Alert.alert(
            'Do you want to edit or delete RSS?',
            '',
            [
                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                {
                    text: 'Edit', onPress: () => {
                        let { rssList } = this.state;
                        rssList = _.without(rssList, rssUrl);

                        this.props.navigation.navigate('new', {
                            isEdit: true,
                            rssUrl: rssUrl,
                        });
                    }
                },
                {
                    text: 'Delete', onPress: () => {
                        let { rssList } = this.state;
                        rssList = _.without(rssList, rssUrl);

                        AsyncStorage.setItem('rssList', JSON.stringify(rssList))
                            .then(() => {
                                this.setState({ rssList });
                            });
                    }, style: 'destructive'
                }
            ]
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <RssList
                    urls={this.state.rssList}
                    isRefreshing={false}
                    handleRefresh={this.getRssList}
                    handleDelete={this.handleDeleteRss}
                    navigation={this.props.navigation}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEE'
    }
});

export default HomeScreen