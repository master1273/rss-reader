import React from 'react';
import { ScrollView, Button, AsyncStorage, View, Text, StyleSheet, RefreshControl, TouchableOpacity } from 'react-native';
import _ from 'underscore';
import { withNavigationFocus } from 'react-navigation';

class FaveScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            favList: [],
            isRefreshing: false
        };

        this.getFavList();

        this.getFavList = this.getFavList.bind(this);
        this.handleSaveFav = this.handleSaveFav.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
    }

    componentDidMount() {
        this.getFavList();
    }

    componentWillReceiveProps() {
        this.getFavList();
    }
    getFavList() {
        AsyncStorage.getItem('favList')
            .then((response) => {
                console.log("response", response);
                const favList = JSON.parse(response) || [];
                this.setState({ favList,isRefreshing: false });
            });
    }

    handleSaveFav(favItem) {
        const { favList } = this.state;
        if (favItem && !_.contains(favList, favItem)) {

            console.log("add favItem.title", favItem);
            favList.push(favItem);

            AsyncStorage.setItem('favList', JSON.stringify(favList))
                .then(() => {

                    this.getFavList();
                });

        } else {
            console.log("remove favItem.title", favItem);
            var index = favList.indexOf(favItem || null);
            this.setState({
                favList: favList.splice(index, 1)
            });

            AsyncStorage.setItem('favList', JSON.stringify(favList))
                .then(() => {
                    this.getFavList();
                });
        }
    }

    handleRefresh() {
        this.getFavList();
    }

    render() {
        const { favList, isRefreshing } = this.state;
        return (
            <ScrollView style={styles.container}>
                {favList &&
                favList.map((item, index) => (
                    <TouchableOpacity key={index} onPress={() => this.handleSaveFav(item)}>
                        <View>
                            <Text style={styles.title} numberOfLines={2} >
                                {item}
                            </Text>

                            {/* <Text style={styles.footer}>
                                    {entry.author}
                                </Text> */}
                        </View>
                    </TouchableOpacity>
                ))
                }
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        backgroundColor: '#FFF',
        width: "100%",
        borderBottomWidth: 1,
        borderColor: '#DDD'
    },

    textInput: {
        height: 50,
        fontSize: 16
    },

    iconContainer: {
        marginRight: 10,
        padding: 5
    }
});

export default withNavigationFocus(FaveScreen);