import React from 'react';
import { AsyncStorage, View, Text, TextInput, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { _ } from 'underscore';


class NewScreen extends React.Component {
    constructor() {
        super();

        this.state = {
            rssUrl: '',
            rssList: []
        };

        this.getRssList();

        this.getRssList = this.getRssList.bind(this);
        this.handleSaveRss = this.handleSaveRss.bind(this);
        this.handleChangeRssUrl = this.handleChangeRssUrl.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }
    componentDidMount() {
        this.setState({
            isEdit: this.props.navigation.getParam('isEdit', []),
            rssUrl: this.props.navigation.getParam('rssUrl', [])
        });
    }

    getRssList() {
        AsyncStorage.getItem('rssList')
            .then((response) => {
                console.log("response", response);
                const rssList = JSON.parse(response) || [];
                this.setState({ rssList });
            });
    }

    handleSaveRss() {
        const { rssList, rssUrl } = this.state;
        if (rssUrl.length) {
            if (!_.contains(rssList, rssUrl)) {
                rssList.push(rssUrl);

                AsyncStorage.setItem('rssList', JSON.stringify(rssList))
                    .then(() => {
                        this.setState({
                            rssList
                        });
                        this.props.navigation.navigate('home');
                    });
            }
        } else {
            Alert.alert(
                'Error while saving',
                '',
                [
                    { text: 'OK', onPress: () => { }, style: 'cancel' }
                ]
            );

        }
    }

    handleChangeRssUrl(rssUrl) {
        this.setState({ rssUrl });
    }

    handleCancel() {
        this.props.navigation.goBack();
    }

    render() {
        const { rssUrl } = this.state;
        return (

            <View style={styles.inputContainer}>
                <TextInput
                    style={{ height: 80 }}
                    placeholder={'Add new RSS URL...'}
                    value={rssUrl || ''}
                    onChangeText={this.handleChangeRssUrl}
                    autoCorrect={false}
                    autoCapitalize="none"
                    underlineColorAndroid={'transparent'}
                    paddingRight={12}
                    paddingLeft={12}
                />

                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.button} onPress={this.handleSaveRss}>
                        <Text style={styles.buttonText}>Save</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonCancel} onPress={this.handleCancel}>
                        <Text style={styles.buttonText}>Cancel</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        backgroundColor: '#FFF',
        width: "100%",
        borderBottomWidth: 1,
        borderColor: '#DDD'
    },

    textInput: {
        height: 50,
        fontSize: 16,
        paddingVertical: 0
    },
    button: {
        backgroundColor: '#5251ff',
        width: '30%',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonCancel: {
        backgroundColor: 'red',
        width: '30%',
        height: 40,
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: '#fff'
    },

    iconContainer: {
        marginRight: 10,
        padding: 5
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 70
    }
});

export default NewScreen;