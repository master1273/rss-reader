import React from 'react';
import { View, AsyncStorage, ScrollView, Text, Alert, RefreshControl, StyleSheet } from 'react-native';
import _ from 'underscore';
import RssItem from '../component/RssItem';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEE'
    },
    error: {
        color: '#777',
        textAlign: 'center',
        marginTop: 15
    }
});

const RssList = (props) => {

    const renderRssItems = () => props.urls.map((url, index) => (
        <RssItem
            key={url}
            url={url}
            handleDelete={props.handleDelete}
            navigation={props.navigation}
        />
    ));

    return (
        <ScrollView
            style={styles.container}
            refreshControl={
                <RefreshControl
                    refreshing={props.isRefreshing}
                    onRefresh={props.handleRefresh}
                />
            }
        >
            { props.urls.length ?
                renderRssItems() :
                <Text style={styles.error}>No RSS.</Text>
            }
        </ScrollView>
    );
};

export default class ManagerScreen extends React.Component {

    static navigationOptions = {
        title: 'Home'
    };

    constructor() {
        super();

        this.state = {
            rssUrl: '',
            rssList: []
        };

        this.getRssList();

        this.getRssList = this.getRssList.bind(this);
        this.handleDeleteRss = this.handleDeleteRss.bind(this);
    }

    getRssList() {
        AsyncStorage.getItem('rssList')
            .then((response) => {
                const rssList = JSON.parse(response) || [];
                this.setState({ rssList });
            });
    }

    handleDeleteRss(rssUrl) {
        Alert.alert(
            'Edit or Delete RSS?',
            '',
            [
                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                {
                    text: 'Edit', onPress: () => {
                        let { rssList } = this.state;
                        rssList = _.without(rssList, rssUrl);

                        this.props.navigation.navigate('new', {
                            isEdit: true,
                            rssUrl: rssUrl,
                        });
                    }
                },
                {
                    text: 'Delete', onPress: () => {
                        let { rssList } = this.state;
                        rssList = _.without(rssList, rssUrl);

                        AsyncStorage.setItem('rssList', JSON.stringify(rssList))
                            .then(() => {
                                this.setState({ rssList });
                            });
                    }, style: 'destructive'
                }
            ]
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <RssList
                    urls={this.state.rssList}
                    isRefreshing={false}
                    handleRefresh={this.getRssList}
                    handleDelete={this.handleDeleteRss}
                    navigation={this.props.navigation}
                />
            </View>
        );
    }

}