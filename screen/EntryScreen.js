import React, { PropTypes } from 'react';
import { View } from 'react-native';
import EntryDetail from "../component/EntryDetail";

class EntryScreen extends React.Component {
    render() {
        const { navigation } = this.props;
        const entry = navigation.getParam('entry', {});

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <EntryDetail entry={entry} />
            </View>
        );
    }
}

EntryScreen.route = {
    navigationBar: {
        visible: true,
        title(params) {
            return params.entry.title;
        },
        backgroundColor: '#cdcdcd',
        tintColor: '#FFF'
    }
};

export default EntryScreen;
