import React, { PropTypes } from 'react';
import { View, StyleSheet } from 'react-native';
import FeedList from '../component/FeedList';

class FeedScreen extends React.Component {
    static navigationOptions = {
        title: 'Home'
    };
    constructor() {
        super();

        this.state = {
        };
    }

    render() {
        const entries = this.props.navigation.getParam('entries', []);

        return (
            <View style={styles.container}>
                <FeedList entries={entries} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEE'
    }
});

export default FeedScreen;
