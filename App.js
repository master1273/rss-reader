import React from 'react';
import AppNavigator from './util/AppNavigator';
import { StyleSheet} from 'react-native';
import { createNavigationContainer  } from 'react-navigation';

const AppContainer = createNavigationContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
