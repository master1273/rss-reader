import React from 'react';
import { DrawerActions, createStackNavigator, createDrawerNavigator, createMaterialTopTabNavigator } from 'react-navigation';
import { Icon } from "native-base";

import FaveScreen from '../screen/FaveScreen';
import TagScreen from '../screen/TagScreen';
import DrawerScreen from '../screen/DrawerScreen';
import FeedScreen from '../screen/FeedScreen';
import EntryScreen from '../screen/EntryScreen';
import NewScreen from '../screen/NewScreen';
import HomeScreen from "../screen/HomeScreen";


const HomeTabs = createMaterialTopTabNavigator({
    home: HomeScreen,
    faves: FaveScreen
}, {
    tabBarOptions: {
        activeTintColor: '#fff',
        inactiveTintColor: '#bfbfbf',
        style: {
            backgroundColor: '#5251ff',
        },
        indicatorStyle: {
            backgroundColor: '#fff',
        },
    }
});


const DrawerNavigator = createDrawerNavigator({
    main: { screen: HomeTabs },
    tags: { screen: TagScreen },
    feed: { screen: FeedScreen},
    entry: {
        screen: EntryScreen,
        navigationOptions: ({ navigation }) => ({
            title: navigation.getParam('entry', { title: '' }).title
        })
    },
    new: { screen: NewScreen, navigationOptions: { title: 'Add or Edit RSS' } }
}, {
    initialRouteName: 'main',
    contentComponent: DrawerScreen,
    drawerWidth: 300
});

const StackNavigator = createStackNavigator({
    DrawerNavigator: {
        screen: DrawerNavigator
    }
}, {
    navigationOptions: ({ navigation }) => ({
        title: 'RSS Reader',
        headerStyle: {
            backgroundColor: '#5251ff',
        },
        headerLeft: <Icon name="menu" style={{ margin: 10, color: '#fff' }} onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}/>,
        headerRight: <Icon name="add" style={{ margin: 10, color: '#fff' }} onPress={() => navigation.navigate('new', {
            isEdit: false,
            rssUrl: '',
        })} />,
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        }
    })
});

export default StackNavigator