import RssItem from "./RssItem";
import {RefreshControl, ScrollView, StyleSheet, Text} from "react-native";
import React from "react";

const RssList = (props) => {

    const renderRssItems = () => props.urls.map((url, index) => (
        <RssItem
            key={url}
            url={url}
            handleDelete={props.handleDelete}
            navigation={props.navigation}
        />
    ));

    return (
        <ScrollView
            style={styles.container}
            refreshControl={
                <RefreshControl
                    refreshing={props.isRefreshing}
                    onRefresh={props.handleRefresh}
                />
            }>
            { props.urls.length ?
                renderRssItems() :
                <Text style={styles.error}>No RSS.</Text>
            }
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    error: {
        color: 'red',
        textAlign: 'center',
        marginTop: 15
    }
});

export default RssList