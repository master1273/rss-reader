import React, { PropTypes } from 'react';
import { ScrollView, StyleSheet, Text } from 'react-native';
import EntryItem from '../component/EntryItem';

class FeedList extends React.Component {
    render() {
        const { entries, navigation } = this.props;
        return (
            <ScrollView style={styles.container} >
                {
                    entries.map((entry, index) => (
                        <EntryItem key={index} entry={entry} />
                    ))
                }
            </ScrollView>
        );

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 5
    }
});

export default FeedList;
