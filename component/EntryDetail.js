import React, { PropTypes } from 'react';
import { ScrollView, Text, Linking, StyleSheet, AsyncStorage } from 'react-native';
import HTMLView from 'react-native-htmlview';
import { Icon } from 'native-base'
import _ from 'underscore';

const FavoriteImage = ({ entry, isFavorite, onPress }) => {
    return <Icon
        raised
        name='star'
        color={isFavorite ? '#f50' : 'black'}
        onPress={() => onPress(entry)} />
};

class EntryDetail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isFavorite: false,
            favItem: {},
            favList: []
        };

        this.getFavList();

        this.getFavList = this.getFavList.bind(this);
        this.handleSaveFav = this.handleSaveFav.bind(this);
    }

    componentDidMount() {
        this.setState({
            favItem: this.props.entry
        });
    }
    getFavList() {
        AsyncStorage.getItem('favList')
            .then((response) => {
                console.log("response", response);
                const favList = JSON.parse(response) || [];
                this.setState({ favList });
            });
    }

    handleSaveFav(favItem) {
        const { favList } = this.state;
        if (!_.contains(favList, favItem.title)) {

            console.log("add favItem.title", favItem.title);
            favList.push(favItem.title);

            AsyncStorage.setItem('favList', JSON.stringify(favList))
                .then(() => {
                    this.setState({
                        favList
                    });
                });

        } else {
            console.log("remove favItem.title", favItem.title);
            var index = favList.indexOf(favItem.title);
            this.setState({
                favList: favList.splice(index, 1)
            });

            AsyncStorage.setItem('favList', JSON.stringify(favList))
                .then(() => {
                    this.setState({
                        favList
                    });
                });
        }
    }

    render() {
        const { favList } = this.state;
        const { entry } = this.props;
        return (

            <ScrollView style={styles.container}>
                <FavoriteImage isFavorite={_.contains(favList, entry.title)} entry={entry} onPress={this.handleSaveFav} />

                <Text style={styles.title} >
                    {entry.title}
                </Text>

                <Text style={styles.author}>
                    {entry.author}
                </Text>

                <HTMLView
                    value={entry.content}
                    stylesheet={styles}
                    onLinkPress={url => Linking.openURL(url)}
                />
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        marginBottom: 5,
        padding: 7
    },

    title: {
        color: '#555',
        fontSize: 16,
        fontWeight: '600'
    },

    author: {
        color: '#AAA',
        fontSize: 9,
        fontWeight: '500',
        paddingTop: 7
    },

    a: {
        color: 'blue'
    }
});

export default EntryDetail;
